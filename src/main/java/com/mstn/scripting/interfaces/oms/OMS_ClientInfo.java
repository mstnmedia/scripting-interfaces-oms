/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.oms;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Interface_Option;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import java.util.Arrays;
import java.util.List;

/**
 * @author amatos
 */
public class OMS_ClientInfo extends InterfaceConnectorBase {

	private final static int ID = 1001;
	private final static String NAME = "oms_client_info";
	private final static String LABEL = "OMS - Información del Cliente";
	private final static String SERVER_URL = "oms.org";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	public OMS_ClientInfo() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Option> getOptions() {
		return Arrays.asList(
				//(id, id_interface, name, id_type, value, color, order_index)
				new Interface_Option(1, ID, "Encontrado en Claro", 1, "is_claro_number", "green", 1),
				new Interface_Option(2, ID, "No encontrado", 1, "not_found", "red", 2)
		);
	}

	@Override
	public List<Interface_Field> getFields() {
		return Arrays.asList(
				//(id, id_interface, name, label, id_type, order_index, default_value)
				new Interface_Field(1, ID, "phone", "Número telefónico", InterfaceFieldTypes.TEXT, 1, null),
				new Interface_Field(2, ID, "client_document", "Documento de identidad", InterfaceFieldTypes.TEXT, 2, null)
		);
	}

	@Override
	public List<Transaction_Result> getResults() {
		String prefix = NAME + "_";
		return Arrays.asList(
				//(id_interface, name, label)/
				//(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props)
				new Transaction_Result(1, 0, 0, 0, ID, prefix + "clientid", "ID del cliente", "895641", ""),
				new Transaction_Result(2, 0, 0, 0, ID, prefix + "contratoid", "ID del contrato", "874526", ""),
				new Transaction_Result(3, 0, 0, 0, ID, prefix + "client_document", "Cédula del cliente", "402-2663258-0", ""),
				new Transaction_Result(4, 0, 0, 0, ID, prefix + "clientname", "Nombre del cliente", "Abel Matos Moquete",
						"{ \"cssStyles\": \"font-size: 15px; color: red; text-transform: uppercase;\" }"
				),
				new Transaction_Result(5, 0, 0, 0, ID, prefix + "serviceslist", "Lista de servicios",
						"["
						+ "{\"serviceid\":45267,\"id_client\":895641,\"phone\":\"8294961891\",\"product_name\":\"Plan Prepago Conectado\",\"plan_name\":\"Empleados Claro\",\"status\":\"Cancelado\",\"velocity\":\"10Mb/s\",\"activation_date\":\"2014-08-15\"},"
						+ "{\"serviceid\":89562,\"id_client\":895641,\"phone\":\"8294961819\",\"product_name\":\"Fibra óptica\",\"plan_name\":\"Velocidad Superior\",\"status\":\"Activo\",\"velocity\":\"100Mb/s\",\"activation_date\":\"2018-05-01\"}"
						+ "]",
						"{\"notSave\": true}"
				)
		);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload) throws Exception {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();

		Interface inter = getInterface();
		ObjectNode form = JSON.stringToNode(payload.getForm());

		if (form.has("id_client") || form.has("phone") || form.has("client_document")) {
			response.setAction(TransactionInterfaceActions.NEXT_STEP);

			List<Transaction_Result> results = getResults();

			inter.setInterface_results(results);
			response.setInterface(inter);

			// TODO: Obtener o calcular aquí el valor de respuesta correspondiente con las getOptions()
			//		que mueve de paso
			String value = "true";

			response.setValue(value);
		} else {
			response.setAction(TransactionInterfaceActions.ERROR);
			response.setValue("false");
		}
		return response;
	}
}
