/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.oms;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Interface_Option;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import java.util.Arrays;
import java.util.List;

/**
 * @author amatos
 */
public class OMS_ServiceInfo extends InterfaceConnectorBase {

	private final static int ID = 1000;
	private final static String NAME = "oms_service_info";
	private final static String LABEL = "OMS - Información del Servicio";
	private final static String SERVER_URL = "google.com/api2";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	public OMS_ServiceInfo() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Option> getOptions() {
		return Arrays.asList(
				//(id, id_interface, name, id_type, value, color, order_index)
				new Interface_Option(1, ID, "Registrado en Claro", 1, "is_claro_number", "green", 1),
				new Interface_Option(2, ID, "No registrado", 1, "not_found", "red", 2),
				new Interface_Option(3, ID, "Registrado en otra teléfonica", 1, "not_claro_number", "orange", 3)
		);
	}

	@Override
	public List<Interface_Field> getFields() {
		return Arrays.asList(
				//(id, id_interface, name, label, id_type, order_index, default_value)
				new Interface_Field(1, ID, "phone", "Número telefónico", InterfaceFieldTypes.TEXT, 1, null),
				new Interface_Field(2, ID, "client_document", "Documento de identidad", InterfaceFieldTypes.TEXT, 2, null)
		);
	}

	@Override
	public List<Transaction_Result> getResults() {
		String prefix = NAME + "_";
		return Arrays.asList(
				//(id_interface, name, label)/
				//(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props)
				new Transaction_Result(1, 0, 0, 0, ID, prefix + "phone", "Número", "829-496-1819", ""),
				new Transaction_Result(2, 0, 0, 0, ID, prefix + "clientid", "ID del cliente", "895641", ""),
				new Transaction_Result(3, 0, 0, 0, ID, prefix + "clientname", "Nombre del cliente", "Abel Matos Moquete", "{ \"cssStyles\": \"background-color: lightgreen; text-transform: uppercase\" }"),
				new Transaction_Result(4, 0, 0, 0, ID, prefix + "serviceid", "ID del servicio", "89562", ""),
				new Transaction_Result(5, 0, 0, 0, ID, prefix + "productname", "Nombre del producto", "ADSL", ""),
				new Transaction_Result(6, 0, 0, 0, ID, prefix + "velocity", "Velocidad", "10Mb/s", "{ \"cssStyles\": \"background-color: lightgreen;\" }"),
				new Transaction_Result(7, 0, 0, 0, ID, prefix + "status", "Estado", "Activo", "{ \"cssStyles\": \"background-color: lightgreen;\" }")
		);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload) throws Exception {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();

		Interface inter = getInterface();
		ObjectNode form = JSON.stringToNode(payload.getForm());

		if (form.has("phone") || form.has("service_id")) {
			response.setAction(TransactionInterfaceActions.NEXT_STEP);

			List<Transaction_Result> results = getResults();

			inter.setInterface_results(results);
			response.setInterface(inter);

			// TODO: Obtener o calcular aquí el valor de respuesta correspondiente con las getOptions()
			//		que mueve de paso
			String value = "true";

			response.setValue(value);
		} else {
			response.setAction(TransactionInterfaceActions.ERROR);
			response.setValue("false");
		}
		return response;
	}
}
